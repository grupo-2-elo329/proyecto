#ifndef MANGA_H
#define MANGA_H

#include <QPushButton>
#include <QLayout>
#include <QLabel>
#include <QLineEdit>
#include <QGroupBox>
#include <QDialog>
#include <QFormLayout>
#include <QGridLayout>
#include <QFileDialog>
#include <fstream>

#include "stock.h"

class Manga: public QDialog
{
    Q_OBJECT
    private:
        QVBoxLayout* ventana_principal;
        QLineEdit* editTitulo= new QLineEdit(this);;
        QLineEdit* editPrecio= new QLineEdit(this);
        QLineEdit* editPath= new QLineEdit(this);
        QPushButton *botonStock;
        QWidget* window;
        QString precio;
        int volumenes_creados=0;
        QString titulo;
        QString path= "/home/jared/Documents/POO/RIAL_FINAL/no-img.jpg";
        QFormLayout* mainLayout;
        QPushButton* okButton=new QPushButton("OK");
        QPushButton* buscarImagen=new QPushButton("Seleccionar Imagen");
        QHBoxLayout *formatoCaja;
        QVBoxLayout *formatoFoto;
        QGroupBox *formatoTitulo;
        QGridLayout *formatoVolumenes;
        QLabel* lbl;
        //QPushButton *Imagen; //cambiar por imagen
    public:
        Manga(QWidget* ,QVBoxLayout *);
        Manga(QVBoxLayout*,QWidget* ,QString);
        ~Manga();
        QList<stock*> volumenes;
        void mangaHide();
        void mangaShow();
        /* Getters*/
        QString getTitulo(){return titulo;}
        QString getPath(){return path;}
        QString getPrecio(){return precio;}
    public slots:
        void crearItem();
        void pathImagen();
        void crearStock();
};

#endif // MANGA_H
