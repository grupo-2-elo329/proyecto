#include "ventana_remover.h"


Ventana_remover::Ventana_remover(QWidget* parent) : QDialog(parent){
    //this->inventario = parent;
    this->setWindowTitle("Elimina un manga de tu lista"); //nombre de la ventana
    this->setFixedSize(450,100); //size dee la ventana

    this->mainLayout = new QFormLayout(this); //se crea el layout de la ventana
    /* Filas del layout */
    this->Titulo = new QLineEdit(this); //wea donde se escribe el titulo del manga que quieres borrar
    Titulo->setPlaceholderText("Borra tu manga...");
    this->okButton = new QPushButton("OK"); //boton de ok
    this->cancelButton = new QPushButton("cancel"); //boton cancel
    this->mainLayout->addRow("Título del Manga",this->Titulo); //agrega en el layout la wea pa escribir
    this->mainLayout->addRow(this->okButton,this->cancelButton); //agrega boton al layout
    this->okButton->setFixedWidth(100);  //setea size fijo
    this->cancelButton->setFixedWidth(100);

    connect(this->okButton,SIGNAL(clicked()),parent,SLOT(borrarManga())); //para buscar la wea pa eliminar
    connect(this->okButton,SIGNAL(clicked()),this,SLOT(accept()));
    connect(this->okButton,SIGNAL(clicked()),this->Titulo,SLOT(clear()));//para que cuando se aprete el boton se borree lo que estaba antes
    connect(this->cancelButton,SIGNAL(clicked()),this,SLOT(close())); // para cerrar la ventana
    connect(this->cancelButton,SIGNAL(clicked()),this->Titulo,SLOT(clear()));

}


QLineEdit *Ventana_remover::getTitulo(){
    return this->Titulo;
}

