#include "manga.h"
#include <iostream>



Manga::Manga(QVBoxLayout* ventana, QWidget* window, QString linea){

    this->window=window;
    this->ventana_principal=ventana;
    QStringList datos=linea.split(QChar(','));

    this->editTitulo->setText(QString(datos.at(0)));
    //editPath->setText(Path);
    this->path=QString(datos.at(1));
    this->editPrecio->setText(QString(datos.at(2)));
    crearItem();
    int i=3;
    while(i<datos.size()-1){

        stock* volumen=new stock(this->formatoVolumenes,datos.at(i),datos.at(i+1).toInt(),datos.at(i+2), this->volumenes_creados);
        this->volumenes.append(volumen);
        this->volumenes_creados++;
        i+=3;
    }
}
Manga::Manga(QWidget* window,QVBoxLayout* ventana)
{
    this->window=window;
    this->ventana_principal=ventana;
    this->setWindowTitle("Añadir nuevo Manga");
    this->setFixedSize(450,150);
    QFormLayout* mainLayout = new QFormLayout(this);
    mainLayout->setFormAlignment(Qt::AlignCenter);

    QLabel *labelTitulo= new QLabel(); //instrucciones para poner el titulo
    labelTitulo->setText("Título del Manga:" );

    QLabel *labelPrecio=new QLabel(); //instrucciones para poner el precio
    labelPrecio->setText("Fije un precio de Venta");

    QLabel *labelImagen= new QLabel();//instrucciones para poner el path
    labelImagen->setText("Dirección de la imagen:" );
    QHBoxLayout* imagenLayout=new QHBoxLayout();
    imagenLayout->addWidget(this->editPath);
    imagenLayout->addWidget(this->buscarImagen);
    mainLayout->addRow(labelTitulo,this->editTitulo);
    mainLayout->addRow(labelPrecio, this->editPrecio);
    mainLayout->addRow(labelImagen,imagenLayout);
    mainLayout->addWidget(this->okButton);
    this->okButton->setFixedWidth(100);

    connect(this->buscarImagen, SIGNAL(clicked()), this, SLOT(pathImagen()));
    connect(this->okButton,SIGNAL(clicked()),this,SLOT(accept()));
    connect(this,SIGNAL(accepted()),this,SLOT(crearItem()));
    this->show();
}
//Destructor
Manga::~Manga(){
    if(editTitulo!=NULL) delete this->editTitulo;
    if(editPrecio!=NULL) delete this->editPrecio;
    if (botonStock!=NULL)delete this->botonStock;
    //if (mainLayout!=NULL)delete this->mainLayout;
    if (okButton!=NULL)delete this->okButton;
    if (buscarImagen!=NULL)delete this->buscarImagen;
    if (formatoVolumenes!=NULL)delete this->formatoVolumenes;
    if (formatoTitulo!=NULL)delete this->formatoTitulo;
    delete this->lbl;

}

void Manga::crearItem(){
    this->titulo=editTitulo->text();
    this->precio=editPrecio->text();
    this->formatoCaja=new QHBoxLayout(); //caja que contiene todo
    this->formatoFoto= new QVBoxLayout(); //caja que contiene foto y boton
    this->formatoTitulo= new QGroupBox(); //caja que contiene titulo y grid
    this->formatoVolumenes= new QGridLayout(); //el grid
    this->formatoTitulo->setTitle(titulo);
    this->formatoTitulo->setLayout(this->formatoVolumenes);
    this->lbl = new QLabel();  // set content to show center in label
    lbl->setAlignment(Qt::AlignCenter);
    QPixmap pix;    // to check wether load ok
    if(pix.load(this->path)){
          // scale pixmap to fit in label'size and keep ratio of pixmap
          pix = pix.scaled(150,150, Qt::KeepAspectRatio);
          lbl->setPixmap(pix);}
    this->botonStock = new QPushButton("Agregar Volumen");
    this->botonStock->setFixedWidth(lbl->pixmap()->width());
    this->formatoTitulo->setFixedWidth(this->window->width()-this->botonStock->width()-40);
    this->formatoFoto->addWidget(lbl);
    this->formatoFoto->addWidget(botonStock);
    this->formatoCaja->addLayout(this->formatoFoto);
    this->formatoCaja->addWidget(this->formatoTitulo);
    this->formatoCaja->setAlignment(Qt::AlignLeft);
    this->ventana_principal->addLayout(this->formatoCaja);

    connect(this->botonStock, SIGNAL(clicked()), this, SLOT(crearStock()));
}

void Manga::pathImagen(){
    QString path=QFileDialog::getOpenFileName(this,tr("Seleccione una Imagen"), "C:/", tr("Image Files (*.png *.jpg *.bmp *.jpeg)"));
    if(path!=NULL){
        this->path=path;
        this->editPath->setText(path);
    }
}

void Manga::crearStock(){
    stock* volumen=new stock(this->formatoVolumenes, this->precio, this->volumenes_creados);
    this->volumenes.append(volumen);
    this->volumenes_creados++;
}

void Manga::mangaHide(){
    this->hide();
    this->editTitulo->hide();
    this->editPrecio->hide();
    this->botonStock->hide();
    this->okButton->hide();
    this->buscarImagen->hide();
    this->formatoTitulo->hide();
    this->lbl->hide();
}

void Manga::mangaShow(){
    //this->editTitulo->show();
    //this->editPrecio->show();
    this->botonStock->show();
    //this->okButton->show();
    //this->buscarImagen->show();
    this->formatoTitulo->show();
    this->lbl->show();
}
