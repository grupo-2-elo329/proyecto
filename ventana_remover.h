#ifndef VENTANA_REMOVER_H
#define VENTANA_REMOVER_H

#include <QDialog>
#include <QWidget>
#include <QFormLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QLayout>
#include <QString>
#include<string.h>

class Ventana_remover : public QDialog {
    Q_OBJECT
public:
    Ventana_remover(QWidget*parent = Q_NULLPTR);
    //~Ventana_remover(); ni idea porque pero esta linea genera una cantidad de bugs impresionante
    QLineEdit *getTitulo();
private:
    QFormLayout* mainLayout;
    QLineEdit* Titulo;
    QPushButton* okButton;
    QPushButton* cancelButton;
public slots:
    //void borrarManga();
};

#endif // VENTANA_REMOVER_H
