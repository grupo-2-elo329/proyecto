#ifndef STOCK_H
#define STOCK_H

#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QDialog>
#include <QFormLayout>
#include <QGridLayout>
#include <QLabel>
#include <QSpinBox>

class stock:public QDialog
{
    Q_OBJECT
public:
    stock(QGridLayout*, QString, int);
    stock(QGridLayout*, QString, int, QString, int);
    QGridLayout* grid_principal;
    /*Getters*/
    QString getNumero_volumen(){return numero_volumen;}
    int getCantidad(){return cantidad;}
    QString getPrecio(){return precio;}
public slots:
    void crearVolumen();
    void ventanaVolumen();
    void modificarVolumen();
private:
    QLineEdit* editNumero= new QLineEdit();
    QLineEdit* editPrecio= new QLineEdit();
    QLineEdit* editCantidad= new QLineEdit();
    QFormLayout* mainLayout;
    QPushButton* okButton; //confirmar configuracion inicial
    QPushButton* botonGuardar; //confirmar modificacion de volumen
    QPushButton* nuevoBoton;
    QString precio;
    int cantidad;
    int creados;
    QString numero_volumen;
    QSpinBox* CantidadSpinner;
    QDialog* ventanaV; //ventana que se abre al modificar volumen
    QFormLayout* layout;//ventana que se abre al modificar volumen

};

#endif // STOCK_H
