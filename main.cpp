#include "inventario_manga.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Inventario_Manga w;

    /*Setup Inicial Memoria*/
    //if archivo no vacio

    QFile file(QString("memoria.csv"));
    if(file.open(QIODevice::ReadWrite|QIODevice::Text))
    {
        QTextStream in(&file);
        while(!in.atEnd()){
            QString line=in.readLine();
            w.inventarioInit(line);
        }
    }
    file.close();
    w.show();
    return a.exec();
}
