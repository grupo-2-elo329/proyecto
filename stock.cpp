#include "stock.h"
#include <iostream>
stock::stock(QGridLayout* grid, QString Tomo, int cantidad, QString precio, int creados){

    this->grid_principal=grid;
    this->editNumero->setText(Tomo);
    this->editCantidad->setText(QString::number(cantidad));
    this->editPrecio->setText(precio);
    this->creados=creados;
    crearVolumen();
}
stock::stock(QGridLayout* grid, QString precio, int creados)
{
    this->grid_principal= grid;
    this->precio=precio;
    this->creados=creados;
    this->setWindowTitle("Añadir nuevo Volumen");
    this->setFixedSize(450,150);
    this->mainLayout = new QFormLayout(this);
    this->mainLayout->setFormAlignment(Qt::AlignLeft);

    QLabel *labelNumero= new QLabel(); //instrucciones para poner el titulo
    labelNumero->setText("Numero del Volumen:" );
    //this->editNumero = new QLineEdit();

    QLabel *labelPrecioMod=new QLabel(); //instrucciones para poner el precio
    labelPrecioMod->setText("Modificar el precio original");

    //this->editPrecio= new QLineEdit();
    this->editPrecio->setText(this->precio);


    QLabel *labelCantidad= new QLabel();//instrucciones para poner el path
    labelCantidad->setText("Ingrese cantidad de copias" );
    //this->editCantidad= new QLineEdit();

    this->okButton = new QPushButton("OK");
    this->mainLayout->addRow(labelNumero,this->editNumero);
//    this->mainLayout->addRow(labelPrecioActual, labelPrecio);
    this->mainLayout->addRow(labelPrecioMod, this->editPrecio);
    this->mainLayout->addRow(labelCantidad,this->editCantidad);
    this->mainLayout->addWidget(this->okButton);
    connect(this->okButton,SIGNAL(clicked()),this,SLOT(accept()));
    connect(this,SIGNAL(accepted()),this,SLOT(crearVolumen()));
    this->show();
}

void stock::crearVolumen(){
    this->numero_volumen=editNumero->text();
    this->cantidad=editCantidad->text().toInt();
    if(this->editPrecio->text()!=NULL)this->precio=this->editPrecio->text();
    this->nuevoBoton= new QPushButton("Tomo #"+this->numero_volumen);
    this->grid_principal->addWidget(nuevoBoton, this->creados/10 ,this->creados%10,Qt::AlignLeft);
    connect(this->nuevoBoton,SIGNAL(clicked()), this, SLOT(ventanaVolumen()));
}

void stock::ventanaVolumen(){

    this->ventanaV=new QDialog();
    this->layout= new QFormLayout(this->ventanaV);
    this->ventanaV->setWindowTitle("Tomo #"+this->numero_volumen);
    this->ventanaV->setFixedSize(450,150);
    this->layout->setFormAlignment(Qt::AlignLeft);

    QLabel *labelCantidad= new QLabel();//instrucciones para modificar la cantidad
    labelCantidad->setText("Cantidad de copias");

    this->CantidadSpinner= new QSpinBox();
    this->CantidadSpinner->setValue(this->cantidad);
/*
    QLabel *labelPrecioActual=new QLabel(); //instrucciones para poner el precio
    labelPrecioActual->setText("Precio actual");
    QLabel *labelPrecio=new QLabel(); //instrucciones para poner el precio
    labelPrecio->setNum(this->precio.toInt());
*/

    QLabel *labelPrecioMod=new QLabel(); //instrucciones para poner el precio
    labelPrecioMod->setText("Modificar el precio original");


    this->botonGuardar=new QPushButton("Guardar");
    this->layout->addRow(labelCantidad, this->CantidadSpinner);
//    this->layout->addRow(labelPrecioActual, labelPrecio);
    this->layout->addRow(labelPrecioMod, this->editPrecio);
    this->layout->addWidget(this->botonGuardar);
    connect(this->botonGuardar, SIGNAL(clicked()), this, SLOT(accept()));
    connect(this, SIGNAL(accepted()),this,SLOT(modificarVolumen()));
    this->ventanaV->show();

}

void stock::modificarVolumen(){
    this->cantidad=this->CantidadSpinner->value();
    if(this->editPrecio->text()!=NULL)this->precio=this->editPrecio->text();
    this->ventanaV->close();
}
