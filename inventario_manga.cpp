#include "inventario_manga.h"
#include "manga.h"

#define MIN_WINDOW_WIDTH 800
#define MIN_WINDOW_HEIGHT 600

#include <QToolBar>

/* Constructor */
Inventario_Manga::Inventario_Manga(QWidget *parent)
    : QMainWindow(parent)
{
    /* Setup ventana */
    this->setMinimumWidth(MIN_WINDOW_WIDTH);
    this->setMinimumHeight(MIN_WINDOW_HEIGHT);
    this->window = new QWidget(this);
    this->mainScrollArea=new QScrollArea(this);
    this->mainScrollArea->setWidget(this->window);
    this->window ->resize(this->width()-20, MIN_WINDOW_HEIGHT-20);//-20 para que no choque con la scrollbar
    /* Setup botones */
    this->botonAgregar= new QPushButton("Agregar");
    this->botonRemover= new QPushButton("Remover");
    connect(this->botonAgregar, SIGNAL(clicked()), this, SLOT(Agregar()));
    connect(this->botonRemover, SIGNAL(clicked()), this, SLOT(slotRemover()));
    /* Setup buscador */
    QLabel *labelBuscar= new QLabel();
    labelBuscar->setText("    Buscar  ");
    this->palabraBuscada= new QLineEdit();
    this->palabraBuscada->setPlaceholderText("Buscar manga...");
    /* Setup layouts */
    this->principal= new QVBoxLayout(this->window);
    this->principal->setAlignment(Qt::AlignTop);
    this->principal->setSizeConstraint(QLayout::SetNoConstraint);
    this->setWindowTitle("Inventario de Mangas");
    this->setCentralWidget(this->mainScrollArea);
    /* Setup ToolBar */
    this->topToolBar = new QToolBar("Funcionalidades",this);
    this->topToolBar->setMovable(false);
    topToolBar->addWidget(this->botonAgregar);
    topToolBar->addWidget(this->botonRemover);
    topToolBar->addSeparator();
    topToolBar->addWidget(labelBuscar);
    topToolBar->addWidget(this->palabraBuscada);
    this->addToolBar(Qt::TopToolBarArea,topToolBar);
    /* Setup Remover*/
    this->window_remove= new Ventana_remover(this);
    connect(palabraBuscada,SIGNAL(textEdited(QString)),this,SLOT(busqueda()));

}
/* Destructor */
Inventario_Manga::~Inventario_Manga(){}

void Inventario_Manga::inventarioInit(QString linea){
    this->mangas.append(new Manga(this->principal, this->window, linea));
    this->window->resize(this->window->width(), this->mangas.first()->height()*(this->mangas.size()));
}
/* Slots */
void Inventario_Manga::Agregar(){
    this->mangas.append(new Manga(this->window,this->principal));
    this->window->resize(this->window->width(), this->mangas.last()->height()*(this->mangas.size()+1));
}

void Inventario_Manga::slotRemover(){
    this->window_remove->show();

}

void Inventario_Manga::borrarManga(){
    QString title = this->window_remove->getTitulo()->text();
    std::cout << this->mangas.size() << std::endl;
    for(int i = 0 ; i < this->mangas.size(); i++){
        if(this->mangas.at(i)->getTitulo().toStdString() == title.toStdString()){
            mangas.at(i)->~Manga();
            mangas.removeAt(i);
            break;
        }
    }
    if(!mangas.isEmpty()) this->window->resize(this->window->width(), this->mangas.last()->height()*(this->mangas.size()+1));
}

void Inventario_Manga::busqueda(){

    this->hint = palabraBuscada->text().toStdString(); //sin esta variable occurre un warning
    int arreglo[mangas.size()];
    for(int k = 0; k <mangas.size(); k++) arreglo[k] = 0;
    for(int j = 0; j < mangas.size() ; j++){ //se mueve entre mangas
        this->dataManga = mangas.at(j)->getTitulo().toStdString();
        this->dataBuscador = this->hint;
        for(int i = 0; i < (int)this->hint.size(); i++){  //se mueve entre letras
            if(this->dataBuscador[i] == dataManga[i]){
                arreglo[j] += 1; //menos mal ctm
            }
        }
    }

    for(int k = 0; k < mangas.size(); k++){
        if(arreglo[k] != (int)this->hint.size()){
            mangas.at(k)->mangaHide();
        } else {
            mangas.at(k)->mangaShow();
        }
    }
    for(int k = 0; k <mangas.size(); k++) arreglo[k] = 0;

}

void Inventario_Manga::closeEvent(QCloseEvent *) {//logica c:
    std::ofstream file;
    file.open("memoria.csv",std::ofstream::trunc);
    for(int i = 0; i < mangas.size(); i++){
        file<<mangas.at(i)->getTitulo().toStdString()<<",";
        file<<mangas.at(i)->getPath().toStdString()<<",";
        file<<mangas.at(i)->getPrecio().toStdString()<<",";
        for(int j = 0; j < mangas.at(i)->volumenes.size(); j++){
            file<<mangas.at(i)->volumenes.at(j)->getNumero_volumen().toStdString()<<",";
            file<<mangas.at(i)->volumenes.at(j)->getCantidad()<<",";
            file<<mangas.at(i)->volumenes.at(j)->getPrecio().toStdString()<<",";
        }
        file<<std::endl;
        //file<<mangas.at(i)->.toStdString()<<",";
    }
    file.close();
}

