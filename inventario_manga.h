#ifndef INVENTARIO_MANGA_H
#define INVENTARIO_MANGA_H

/* Qt libs */
#include <QMainWindow>
#include <QScrollArea>
#include <QPushButton>
#include <QLayout>
#include <QLabel>
#include <QLineEdit>
#include <fstream>
#include <iostream>
#include <QFile>
#include <QTextStream>
/* Dev libs */
#include "manga.h"
#include "ventana_remover.h"

class Inventario_Manga : public QMainWindow
{
    Q_OBJECT

public:
    Inventario_Manga(QWidget *parent = nullptr);
    ~Inventario_Manga();
    void inventarioInit(QString);
    std::string hint;
    std::string dataBuscador;
    std::string dataManga;

private:
    /* Ventana */
    QWidget *window;
    QScrollArea* mainScrollArea;
    Ventana_remover *window_remove;
    /* Layouts */
    QVBoxLayout* principal;
    /* Botones */
    QPushButton *botonAgregar;
    QPushButton *botonRemover;
    /* Buscar */
    QLineEdit *palabraBuscada;
    /* Data */
    QList<Manga*> mangas;
    /* Top ToolBar */
    QToolBar *topToolBar;



protected:
    void closeEvent(QCloseEvent *);

private slots:
    void slotRemover();
    void Agregar();
    void borrarManga();
    void busqueda();

};
#endif // INVENTARIO_MANGA_H
